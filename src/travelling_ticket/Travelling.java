package travelling_ticket;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Travelling {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Travelling window = new Travelling();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Travelling() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 0, 1370, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.CYAN);
		panel.setBackground(Color.RED);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(346, 10, 570, 81);
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel = new JLabel("Yatra");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBackground(Color.PINK);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 65));
		panel.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 113, 1336, 2);
		frame.getContentPane().add(separator);
		
		JLabel lblNewLabel_1 = new JLabel("India Ka Travel Partner");
		lblNewLabel_1.setForeground(new Color(220, 20, 60));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 32));
		lblNewLabel_1.setBounds(437, 113, 662, 62);
		frame.getContentPane().add(lblNewLabel_1);
		
		
		String url="p1.png";
		ImageIcon i1=new ImageIcon(url);
		JLabel lblNewLabel_2 = new JLabel(i1,JLabel.CENTER);
		lblNewLabel_2.setBounds(50, 150, 300, 300);
		frame.getContentPane().add(lblNewLabel_2);
		
		String url1="p2.png";
		ImageIcon i2=new ImageIcon(url1);
		JLabel lblNewLabel_3 = new JLabel(i2,JLabel.CENTER);
		lblNewLabel_3.setBounds(350, 180, 300, 300);
		frame.getContentPane().add(lblNewLabel_3);
		
		
		String url2="p3.png";
		ImageIcon i3=new ImageIcon(url2);
		JLabel lblNewLabel_4 = new JLabel(i3,JLabel.CENTER);
		lblNewLabel_4.setBounds(380, 400, 300, 300);
		frame.getContentPane().add(lblNewLabel_4);
		
		
		String url3="p4.png";
		ImageIcon i4=new ImageIcon(url3);
		JLabel lblNewLabel_5 = new JLabel(i4,JLabel.CENTER);
		lblNewLabel_5.setBounds(700, 300, 300, 300);
		frame.getContentPane().add(lblNewLabel_5);
		
		
		String url4="p5.png";
		ImageIcon i5=new ImageIcon(url4);
		JLabel lblNewLabel_6 = new JLabel(i5,JLabel.CENTER);
		lblNewLabel_6.setBounds(50, 400, 300, 300);
		frame.getContentPane().add(lblNewLabel_6);
		
		
		String url5="p6.png";
		ImageIcon i6=new ImageIcon(url5);
		JLabel lblNewLabel_7 = new JLabel(i6,JLabel.CENTER);
		lblNewLabel_7.setBounds(1010, 400, 300, 300);
		frame.getContentPane().add(lblNewLabel_7);
		
		
		String url6="p7.png";
		ImageIcon i7=new ImageIcon(url6);
		JLabel lblNewLabel_8 = new JLabel(i7,JLabel.CENTER);
		lblNewLabel_8.setBounds(1010, 150, 300, 300);
		frame.getContentPane().add(lblNewLabel_8);
		
		
		
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 22));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select", "Paris", "London", "Manali"}));
		comboBox.setBounds(498, 181, 239, 27);
		frame.getContentPane().add(comboBox);
		
		JButton btnGo = new JButton("Go");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBox.getSelectedItem().toString()=="Paris")
				{
					Paris p=new Paris();
					p.NewScreen();
				}
				if(comboBox.getSelectedItem().toString()=="Manali")
				{
					Manali m=new Manali();
					m.NewScreen1();
				}
				if(comboBox.getSelectedItem().toString()=="London")
				{
					London l=new London();
					l.NewScreen2();
				}
			}
		});
		btnGo.setFont(new Font("Tahoma", Font.PLAIN, 29));
		btnGo.setBounds(750, 175, 75, 37);
		frame.getContentPane().add(btnGo);
	}
}
