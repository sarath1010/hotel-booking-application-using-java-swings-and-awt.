package travelling_ticket;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

public class Manali {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void NewScreen1() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Manali window = new Manali();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Manali() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 0, 1370, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 346, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 91, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 916, SpringLayout.WEST, frame.getContentPane());
		panel.setBackground(Color.RED);
		panel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel = new JLabel("Yatra");
		lblNewLabel.setForeground(new Color(0, 0, 255));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 65));
		panel.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		springLayout.putConstraint(SpringLayout.NORTH, separator, 113, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, separator, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, separator, 1346, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(separator);
		
		JLabel lblNewLabel_1 = new JLabel("India Ka Travel Partner");
		lblNewLabel_1.setForeground(new Color(220, 20, 60));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 113, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_1, 437, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_1, 175, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_1, 1099, SpringLayout.WEST, frame.getContentPane());
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 32));
		frame.getContentPane().add(lblNewLabel_1);
		
		JSeparator separator_1 = new JSeparator();
		springLayout.putConstraint(SpringLayout.NORTH, separator_1, 185, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, separator_1, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, separator_1, 188, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, separator_1, 1356, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(separator_1);
		
		JLabel lblParis = new JLabel("Manali,India");
		lblParis.setForeground(new Color(0, 255, 0));
		springLayout.putConstraint(SpringLayout.NORTH, lblParis, 169, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblParis, 447, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblParis, 294, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblParis, 825, SpringLayout.WEST, frame.getContentPane());
		lblParis.setFont(new Font("Tahoma", Font.ITALIC, 59));
		frame.getContentPane().add(lblParis);
		
		textField = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField, 348, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textField, 151, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, textField, 370, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textField, 359, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblName = new JLabel("NAME");
		springLayout.putConstraint(SpringLayout.NORTH, lblName, 304, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblName, 38, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblName, 91, SpringLayout.WEST, frame.getContentPane());
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblName);
		
		JLabel lblAge = new JLabel("AGE");
		springLayout.putConstraint(SpringLayout.NORTH, lblAge, 346, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblAge, 38, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblAge, 94, SpringLayout.WEST, frame.getContentPane());
		lblAge.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblAge);
		
		textField_1 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 306, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 151, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, textField_1, 328, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textField_1, 359, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		springLayout.putConstraint(SpringLayout.NORTH, comboBox, 304, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 678, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, comboBox, 325, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, comboBox, 0, SpringLayout.EAST, panel);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Taj Hotels", "Green Valley hotel and spa", "Oyo Manali", "Manali Heights"}));
		frame.getContentPane().add(comboBox);
		
		JLabel lblHotel = new JLabel("HOTEL");
		springLayout.putConstraint(SpringLayout.NORTH, lblHotel, 304, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblHotel, 560, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblHotel, 613, SpringLayout.WEST, frame.getContentPane());
		lblHotel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblHotel);
		
		JLabel lblNewLabel_2 = new JLabel("ADDRESS\r\n\r\n");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 386, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 38, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_2, 119, SpringLayout.WEST, frame.getContentPane());
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblNewLabel_2);
		
		textField_2 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 391, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 151, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, textField_2, 472, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textField_2, 359, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Male ");
		springLayout.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton, 514, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton, 146, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, rdbtnNewRadioButton, 535, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, rdbtnNewRadioButton, 227, SpringLayout.WEST, frame.getContentPane());
		rdbtnNewRadioButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Female");
		springLayout.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_1, 514, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton_1, 229, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, rdbtnNewRadioButton_1, 535, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, rdbtnNewRadioButton_1, 310, SpringLayout.WEST, frame.getContentPane());
		rdbtnNewRadioButton_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Other");
		springLayout.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_2, 514, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton_2, 329, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, rdbtnNewRadioButton_2, 535, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, rdbtnNewRadioButton_2, 434, SpringLayout.WEST, frame.getContentPane());
		rdbtnNewRadioButton_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(rdbtnNewRadioButton_2);
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(rdbtnNewRadioButton);
		gender.add(rdbtnNewRadioButton_1);
		gender.add(rdbtnNewRadioButton_2);
		
		JLabel lblNewLabel_3 = new JLabel("GENDER");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_3, 518, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_3, 38, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_3, 106, SpringLayout.WEST, frame.getContentPane());
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblNewLabel_3);
		
		JComboBox comboBox_1 = new JComboBox();
		springLayout.putConstraint(SpringLayout.NORTH, comboBox_1, 348, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, comboBox_1, 678, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, comboBox_1, 369, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, comboBox_1, 0, SpringLayout.EAST, panel);
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3"}));
		frame.getContentPane().add(comboBox_1);
		
		JLabel lblGuests = new JLabel("GUESTS");
		springLayout.putConstraint(SpringLayout.NORTH, lblGuests, 346, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblGuests, 560, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblGuests, 632, SpringLayout.WEST, frame.getContentPane());
		lblGuests.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblGuests);
		
		JLabel lblDate = new JLabel("DATE");
		springLayout.putConstraint(SpringLayout.NORTH, lblDate, 386, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblDate, 560, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblDate, 616, SpringLayout.WEST, frame.getContentPane());
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblDate);
		
		JSpinner spinner = new JSpinner();
		springLayout.putConstraint(SpringLayout.NORTH, spinner, 389, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, spinner, 678, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, spinner, 734, SpringLayout.WEST, frame.getContentPane());
		spinner.setModel(new SpinnerNumberModel(1, 1, 31, 1));
		frame.getContentPane().add(spinner);
		
		JSpinner spinner_1 = new JSpinner();
		springLayout.putConstraint(SpringLayout.NORTH, spinner_1, 391, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, spinner_1, 744, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, spinner_1, 800, SpringLayout.WEST, frame.getContentPane());
		spinner_1.setModel(new SpinnerNumberModel(1, 1, 12, 1));
		frame.getContentPane().add(spinner_1);
		
		JSeparator separator_2 = new JSeparator();
		springLayout.putConstraint(SpringLayout.NORTH, separator_2, 605, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, separator_2, 23, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, separator_2, 1346, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(separator_2);
		
		JRadioButton chckbxNewCheckBox = new JRadioButton("Suite");
		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox, 29, SpringLayout.SOUTH, spinner_1);
		springLayout.putConstraint(SpringLayout.WEST, chckbxNewCheckBox, 314, SpringLayout.EAST, textField_2);
		frame.getContentPane().add(chckbxNewCheckBox);
		
		JRadioButton chckbxNewCheckBox_1 = new JRadioButton("Deluxe");
		springLayout.putConstraint(SpringLayout.SOUTH, chckbxNewCheckBox_1, 0, SpringLayout.SOUTH, textField_2);
		chckbxNewCheckBox_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(chckbxNewCheckBox_1);
		
		JRadioButton chckbxNewCheckBox_2 = new JRadioButton("Ultra");
		springLayout.putConstraint(SpringLayout.WEST, chckbxNewCheckBox_2, 244, SpringLayout.EAST, rdbtnNewRadioButton_2);
		chckbxNewCheckBox_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(chckbxNewCheckBox_2);
		
		JRadioButton chckbxNewCheckBox_3 = new JRadioButton("Standard\r\n");
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_3, 16, SpringLayout.SOUTH, chckbxNewCheckBox_1);
		springLayout.putConstraint(SpringLayout.EAST, chckbxNewCheckBox_3, -470, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, chckbxNewCheckBox_1, 0, SpringLayout.WEST, chckbxNewCheckBox_3);
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_2, 0, SpringLayout.NORTH, chckbxNewCheckBox_3);
		chckbxNewCheckBox_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(chckbxNewCheckBox_3);
		
		ButtonGroup room = new ButtonGroup();
		room.add(chckbxNewCheckBox);
		room.add(chckbxNewCheckBox_1);
		room.add(chckbxNewCheckBox_2);
		room.add(chckbxNewCheckBox_3);
		
		JLabel lblType = new JLabel("TYPE");
		springLayout.putConstraint(SpringLayout.NORTH, lblType, -2, SpringLayout.NORTH, chckbxNewCheckBox);
		springLayout.putConstraint(SpringLayout.WEST, lblType, 0, SpringLayout.WEST, lblHotel);
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblType);
		
		JLabel lblFlights = new JLabel("FLIGHTS");
		springLayout.putConstraint(SpringLayout.NORTH, lblFlights, 17, SpringLayout.SOUTH, separator_1);
		springLayout.putConstraint(SpringLayout.WEST, lblFlights, -279, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblFlights, -358, SpringLayout.NORTH, separator_2);
		springLayout.putConstraint(SpringLayout.EAST, lblFlights, -201, SpringLayout.EAST, frame.getContentPane());
		lblFlights.setFont(new Font("Tahoma", Font.PLAIN, 20));
		frame.getContentPane().add(lblFlights);
		
		JButton btnNewButton = new JButton("Book\r\n");
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, -41, SpringLayout.NORTH, separator_2);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 444, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -6, SpringLayout.NORTH, separator_2);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, 533, SpringLayout.WEST, frame.getContentPane());
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean t1 = textField.getText().toString().equals("");
				boolean t2 = textField_1.getText().toString().equals("");
				boolean t3 = textField_2.getText().toString().equals("");
				if(!t1 && !t2 && !t3 ) {
					
				JOptionPane.showMessageDialog(null, "Your booking is confirmed\n We have emailed you the details and payment link");
				System.exit(JFrame.EXIT_ON_CLOSE);
				
			}
				else
					
				JOptionPane.showMessageDialog(null, "Enter the details");
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(btnNewButton);
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("Business");
		springLayout.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_3, -4, SpringLayout.NORTH, lblName);
		rdbtnNewRadioButton_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(rdbtnNewRadioButton_3);
		
		JRadioButton rdbtnNewRadioButton_4 = new JRadioButton("First ");
		springLayout.putConstraint(SpringLayout.EAST, rdbtnNewRadioButton_3, -6, SpringLayout.WEST, rdbtnNewRadioButton_4);
		springLayout.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_4, -4, SpringLayout.NORTH, lblName);
		rdbtnNewRadioButton_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(rdbtnNewRadioButton_4);
		
		JRadioButton rdbtnNewRadioButton_5 = new JRadioButton("Executive\r\n");
		springLayout.putConstraint(SpringLayout.WEST, rdbtnNewRadioButton_5, 1196, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, rdbtnNewRadioButton_4, -6, SpringLayout.WEST, rdbtnNewRadioButton_5);
		springLayout.putConstraint(SpringLayout.NORTH, rdbtnNewRadioButton_5, -4, SpringLayout.NORTH, lblName);
		rdbtnNewRadioButton_5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(rdbtnNewRadioButton_5);
		
		ButtonGroup suite = new ButtonGroup();
		suite.add(rdbtnNewRadioButton_3);
		suite.add(rdbtnNewRadioButton_4);
		suite.add(rdbtnNewRadioButton_5);
		
		JLabel lblClass = new JLabel("CLASS");
		springLayout.putConstraint(SpringLayout.NORTH, lblClass, -29, SpringLayout.NORTH, rdbtnNewRadioButton_3);
		springLayout.putConstraint(SpringLayout.WEST, lblClass, 0, SpringLayout.WEST, rdbtnNewRadioButton_3);
		springLayout.putConstraint(SpringLayout.SOUTH, lblClass, -7, SpringLayout.NORTH, rdbtnNewRadioButton_3);
		springLayout.putConstraint(SpringLayout.EAST, lblClass, -18, SpringLayout.EAST, lblNewLabel_1);
		lblClass.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frame.getContentPane().add(lblClass);
		
		JComboBox comboBox_2 = new JComboBox();
		springLayout.putConstraint(SpringLayout.NORTH, comboBox_2, -3, SpringLayout.NORTH, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.WEST, comboBox_2, 0, SpringLayout.WEST, rdbtnNewRadioButton_3);
		springLayout.putConstraint(SpringLayout.EAST, comboBox_2, -10, SpringLayout.EAST, rdbtnNewRadioButton_4);
		comboBox_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"IndiGo", "AirIndia", "AirAsia", "Vistara", "Spice Jet"}));
		frame.getContentPane().add(comboBox_2);
		
		JLabel lblAirlines = new JLabel("AIRLINES");
		springLayout.putConstraint(SpringLayout.NORTH, lblAirlines, -2, SpringLayout.NORTH, textField);
		springLayout.putConstraint(SpringLayout.WEST, lblAirlines, 0, SpringLayout.WEST, rdbtnNewRadioButton_3);
		springLayout.putConstraint(SpringLayout.EAST, lblAirlines, -224, SpringLayout.EAST, frame.getContentPane());
		lblAirlines.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblAirlines);
		
		JButton btnSearch = new JButton("Search");
		springLayout.putConstraint(SpringLayout.NORTH, btnSearch, 24, SpringLayout.NORTH, textField_2);
		springLayout.putConstraint(SpringLayout.WEST, btnSearch, 0, SpringLayout.WEST, rdbtnNewRadioButton_4);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Flights2 f4=new Flights2();
				f4.NewScreent();
			}
		});
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(btnSearch);
		
		JLabel lblNewLabel_4 = new JLabel("TRAINS");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_4, 28, SpringLayout.SOUTH, btnSearch);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_4, 1, SpringLayout.WEST, rdbtnNewRadioButton_4);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_4, -119, SpringLayout.EAST, frame.getContentPane());
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 25));
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblClass_1 = new JLabel("CLASS");
		springLayout.putConstraint(SpringLayout.NORTH, lblClass_1, 0, SpringLayout.NORTH, rdbtnNewRadioButton);
		springLayout.putConstraint(SpringLayout.WEST, lblClass_1, 0, SpringLayout.WEST, rdbtnNewRadioButton_3);
		lblClass_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(lblClass_1);
		
		JRadioButton chckbxNewCheckBox_4 = new JRadioButton("1st AC\r\n");
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_4, 5, SpringLayout.SOUTH, lblClass_1);
		chckbxNewCheckBox_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		springLayout.putConstraint(SpringLayout.WEST, chckbxNewCheckBox_4, 0, SpringLayout.WEST, rdbtnNewRadioButton_3);
		frame.getContentPane().add(chckbxNewCheckBox_4);
		
		JRadioButton chckbxNewCheckBox_5 = new JRadioButton("2nd AC");
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_5, 0, SpringLayout.NORTH, chckbxNewCheckBox_4);
		chckbxNewCheckBox_5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(chckbxNewCheckBox_5);
		
		JRadioButton chckbxNewCheckBox_6 = new JRadioButton("3rd AC");
		springLayout.putConstraint(SpringLayout.WEST, chckbxNewCheckBox_6, 1204, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, chckbxNewCheckBox_5, -9, SpringLayout.WEST, chckbxNewCheckBox_6);
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox_6, 0, SpringLayout.NORTH, chckbxNewCheckBox_4);
		chckbxNewCheckBox_6.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(chckbxNewCheckBox_6);
		
		JRadioButton chckbxSl = new JRadioButton("SL");
		springLayout.putConstraint(SpringLayout.NORTH, chckbxSl, 0, SpringLayout.NORTH, chckbxNewCheckBox_4);
		springLayout.putConstraint(SpringLayout.WEST, chckbxSl, 6, SpringLayout.EAST, chckbxNewCheckBox_6);
		chckbxSl.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(chckbxSl);
		
		ButtonGroup trainclass = new ButtonGroup();
		trainclass.add(chckbxNewCheckBox_4);
		trainclass.add(chckbxNewCheckBox_5);
		trainclass.add(chckbxNewCheckBox_6);
		trainclass.add(chckbxSl);
		
		JButton btnNewButton_1 = new JButton("Search");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, -6, SpringLayout.NORTH, separator_2);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_1, -10, SpringLayout.EAST, chckbxNewCheckBox_5);
		frame.getContentPane().add(btnNewButton_1);
	}
}
